# ornamental_fish
Ornamental fish: what are the risks?

MPI requires updated information regarding pathogen susceptibility and temperature tolerances of species approved for import into New Zealand through the Import Health Standard (IHS) Ornamental Fish and Marine Invertebrates.

This RFP relates to a desktop study to prioritise ornamental fish and marine invertebrates for further investigation and assess the literature regarding their pathogen susceptibility and temperature tolerances. 

Findings from this project will be used to update MPI’s risk assessments of pest and pathogen introduction and spread, assist us in prioritising and optimising pathogen testing, and inform our decision making regarding the importation of ornamental fish into New Zealand.  
